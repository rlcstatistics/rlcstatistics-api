variable "docker_image" {
  description = "The path to the Docker image."
  type        = string
}

variable "service_name" {
  description = "The service name."
  type        = string
}

variable "openai_api_key" {
  description = "The OpenAPI Key."
  type        = string
  sensitive   = true
}

variable "jwt_token" {
  description = "The JWT token."
  type        = string
  sensitive   = true
}

variable "env" {
  description = "The environnement."
  type        = string
  default     = "dev"
}