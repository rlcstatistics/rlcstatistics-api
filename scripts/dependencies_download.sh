# Navigate to the root directory of your project.
cd "$(dirname "$0")"/..

# Check if the directory contains a requirements.txt file
if [ -f "requirements.txt" ]; then
    echo "Found requirements.txt, setting up .venv..."

    # Navigate to the directory
    #TODO#16
    # cd src

    # Check if .venv doesn't exist
    if [ ! -d ".venv" ]; then
        # Create a new virtual environment
        python3 -m venv .venv
    fi

    # Activate the virtual environment
    source .venv/bin/activate

    # Install dependencies
    pip install -r requirements.txt

    # Deactivate the virtual environment
    deactivate

    # Navigate back to the root directory
    #TODO#16
    # cd - > /dev/null
else
    echo "requirements.txt not found"
fi