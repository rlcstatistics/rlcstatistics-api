#!/bin/bash

# The location of the VERSION file
VERSION_FILE="./VERSION"

# Check if VERSION file exists. If not, initialize with 0.0.0
if [[ ! -f $VERSION_FILE ]]; then
    echo "0.0.0" > $VERSION_FILE
fi

# Read the current version
CURRENT_VERSION=$(cat $VERSION_FILE)
IFS='.' read -ra VERSION_PARTS <<< "$CURRENT_VERSION"

# Increment the version based on VERSION_LEVEL
case $VERSION_LEVEL in
    major)
        ((VERSION_PARTS[0]++))
        VERSION_PARTS[1]=0
        VERSION_PARTS[2]=0
        ;;
    minor)
        ((VERSION_PARTS[1]++))
        VERSION_PARTS[2]=0
        ;;
    patch)
        ((VERSION_PARTS[2]++))
        ;;
    *)
        echo "Unknown VERSION_LEVEL: $VERSION_LEVEL"
        exit 1
        ;;
esac

# Construct the new version
NEW_VERSION="${VERSION_PARTS[0]}.${VERSION_PARTS[1]}.${VERSION_PARTS[2]}"
echo $NEW_VERSION > $VERSION_FILE

# Get informations
COMMIT_MESSAGE=$(git log --merges -n 1 --pretty=format:'%s')
SOURCE_BRANCH=$(echo $COMMIT_MESSAGE | sed -n "s/Merge branch '\([^']*\)'.*/\1/p")
MERGE_REQUEST_TITLE=$(curl --silent --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests?state=merged&order_by=updated_at&sort=desc&source_branch=$SOURCE_BRANCH" | grep -oPm1 '"title":"\K[^"]+')
CURRENT_DATE=$(date +"%Y-%m-%d %H:%M")

# Update changelog
if [ -f "CHANGELOG.md" ]; then
    cp "CHANGELOG.md" ".CHANGELOG.md"
fi
{
    echo "# Changelog"
    echo ""
    echo "## $NEW_VERSION ($CURRENT_DATE)"
    echo ""
    echo "- $COMMIT_MESSAGE"
    echo "- $MERGE_REQUEST_TITLE"
    echo ""
} > CHANGELOG.md

if [ -f ".CHANGELOG.md" ]; then
    tail -n +3 ".CHANGELOG.md" >> "CHANGELOG.md"
fi

# Commit and push
git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"
git add $VERSION_FILE CHANGELOG.md
git commit -m "Bump version to $NEW_VERSION"
git push "https://root:${ACCESS_TOKEN}@${CI_SERVER_HOST}/$CI_PROJECT_PATH.git" HEAD:main

# Create a new tag for the version
git tag "v$NEW_VERSION"

# Push the tag to the repository
git push "https://root:${ACCESS_TOKEN}@${CI_SERVER_HOST}/$CI_PROJECT_PATH.git" "v$NEW_VERSION"

# Merge to develop
git checkout develop
git merge main
git push "https://root:${ACCESS_TOKEN}@${CI_SERVER_HOST}/$CI_PROJECT_PATH.git" HEAD:develop