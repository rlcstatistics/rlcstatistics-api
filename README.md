# RLCStatistics-API

## Developpement Local

### Installation de l'environnement virtuel

```bash
bash install.sh
```

### Activation de l'environnement virtuel

```
source .venv/bin/activate
```

### Execution des images docker

- Nécessite une session PostGres :
  - Utilisation d'une image docker simple.

```bash
sudo service docker start
sudo docker-compose up --build

sudo service docker start
sudo docker-compose -f docker-compose-postgres.yml up -d
source .venv/bin/activate
uvicorn main:api --host 0.0.0.0 --port 8000 --reload
sudo docker-compose -f docker-compose-postgres.yml down --remove-orphans

sudo docker build -t rlcstatistics-api:latest .
sudo docker run -p 8000:8000 rlcstatistics-api:latest
sudo docker run --env-file .env_docker -p 8000:8000 rlcstatistics-api:latest
```