from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers.users import router_user
from routers.matches import router_matches
from routers.bets import router_bets
from logger import logger


api = FastAPI(
    title="RLCStatistics API", 
    version="1.0.0",
    contact={
        "name": "Adrien Lacaille",
        "url": "https://www.rlcstatistics.net/",
        "email": "adrien.lacaille@gmail.com",
    },
    debug=False,
    swagger_ui_parameters={"defaultModelsExpandDepth": -1})

origins = [
    "https://www.rlcstatistics.net",
    "*"
]

api.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@api.get("/status",response_model=bool, tags=["Utils"])
def get_status():
    logger.info("Is my api work ?")
    return True

api.include_router(router_user, tags=["Users"])
api.include_router(router_matches, tags=["Matches"])
api.include_router(router_bets, tags=["Bets"])