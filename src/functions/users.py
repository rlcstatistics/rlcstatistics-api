from datetime import datetime, timedelta
from schemas.users import *
from schemas.tokens import TokenData

import os
from fastapi import Depends, HTTPException, Security, status

from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from jose import JWTError, jwt

from pydantic import ValidationError

from dao.users import DAOUserGCS
from functions.utils import verify_password

dao_user_s3 = DAOUserGCS()

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="login",
    scopes={
        "me": "Read information about the current user.",
        "admin": "Can do whatever he wants"
    }
)
JWT_SECRET_KEY = os.getenv("JWT_TOKEN", "fake_jwt_token")
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 1440


#### UTILS
def authenticate_user(email: str, password: str, scopes: list) -> UserPassword:
    user = dao_user_s3.get_user_from_email(email=email)
    if not user:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="User doesn't exist")
    if not verify_password(password, user.hashed_password):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Password incorrect")
    if 'admin' in scopes and user.role != "ADMIN":
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Scope unauthorized")
    return user

def create_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, JWT_SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(security_scopes: SecurityScopes, token: str = Depends(oauth2_scheme)):
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
    else:
        authenticate_value = "Bearer"
    credentials_exception = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Could not validate credentials", headers={"WWW-Authenticate": authenticate_value})
    try:
        payload = jwt.decode(token, JWT_SECRET_KEY, algorithms=[ALGORITHM])
        email: str = payload.get("sub")
        if email is None:
            raise credentials_exception
        token_scopes = payload.get("scopes", [])
        token_data = TokenData(scopes=token_scopes, email=email)
    except (JWTError, ValidationError):
        raise credentials_exception
    user = dao_user_s3.get_user_from_email(email=token_data.email)
    if not user:
        raise credentials_exception
    for scope in security_scopes.scopes:
        if scope not in token_data.scopes:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not enough permissions", headers={"WWW-Authenticate": authenticate_value})
    return user

async def get_current_active_user(current_user: User = Security(get_current_user, scopes=["me"])):
    return current_user

async def get_current_active_admin_user(current_user: User = Security(get_current_user, scopes=["admin"])):
    return current_user
