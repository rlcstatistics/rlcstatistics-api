from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import Depends, HTTPException, status
from schemas.users import User, UserLeaderboard
from schemas.tokens import Token
from functions.users import authenticate_user, create_token, get_current_active_admin_user, get_current_active_user
from forms.forms import UserCreateForm
from dao.users import DAOUserGCS

router_user = APIRouter()
dao_user_s3 = DAOUserGCS()

@router_user.post("/user", response_model=User)
async def route_create_user(user_form: UserCreateForm = Depends()):
    return dao_user_s3.create_user(email=user_form.email, username=user_form.username, password=user_form.password)

@router_user.post("/login", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password, form_data.scopes)
    access_token = create_token(data={"sub": user.email, "scopes": form_data.scopes})
    return {"access_token": access_token, "token_type": "Bearer"}

@router_user.get("/user/me", response_model=User)
async def get_my_user(current_user: User = Depends(get_current_active_user)):
    return current_user

@router_user.get("/refill/me", response_model=User)
async def refill_user(current_user: User = Depends(get_current_active_user)):
    response = dao_user_s3.refill_user(user=current_user)
    if response:
        return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Already refill coins for today')

@router_user.delete("/user/me")
async def detele_my_user(current_user: User = Depends(get_current_active_user)):
    dao_user_s3.delete_user_by_email(email=current_user.email)

@router_user.delete("/user")
async def detele_user(email: str, current_user: User = Depends(get_current_active_admin_user)):
    dao_user_s3.delete_user_by_email(email=email)

@router_user.get("/leaderboard", response_model=list[UserLeaderboard])
async def get_leaderboard():
    return dao_user_s3.get_leaderboard()
