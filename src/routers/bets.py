from fastapi import APIRouter, Depends, HTTPException, status
from functions.users import get_current_active_user
from schemas.users import User, Bet
from forms.forms import UserBetForm
from dao.users import DAOUserGCS
from typing import Dict


router_bets = APIRouter()
dao_user_s3 = DAOUserGCS()

@router_bets.get("/bets/me", response_model=list[Bet])
def get_my_bets(current_user: User = Depends(get_current_active_user)):
    return dao_user_s3.get_user_bets(current_user.email)

@router_bets.post("/bet", response_model=Bet)
def add_bet(bet_form: UserBetForm = Depends(), current_user: User = Depends(get_current_active_user)):
    response = dao_user_s3.do_bet(user=current_user, id_match=bet_form.id_match, team_name=bet_form.team_name, amount=bet_form.amount)
    if response:
        return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Bet not allow')