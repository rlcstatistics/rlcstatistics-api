from typing import Optional
from datetime import datetime
from fastapi import APIRouter, HTTPException, status, Query
from fastapi.responses import StreamingResponse
from io import BytesIO
from schemas.matches import UpcomingMatch, Team, UpcomingMatchInfo, Match
from dao.matches import DAOMatchesGCS


router_matches = APIRouter()
dao_matches_s3 = DAOMatchesGCS()

@router_matches.get("/upcoming_matches", response_model=Optional[list[UpcomingMatch]])
def get_upcoming_matches():
    response = dao_matches_s3.get_upcoming_matches()
    if response: return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='No Upcoming Matches')

@router_matches.get("/upcoming_match/{id_match}", response_model=UpcomingMatchInfo)
def get_upcoming_match(id_match: str):
    response = dao_matches_s3.get_upcoming_match(id_match=id_match)
    if response: return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Unknown Match')

@router_matches.get("/team", response_model=Team)
def get_team_info(team_name: str):
    response = dao_matches_s3.get_infos_team(team_name=team_name)
    if response: return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Team Unknown")
    
@router_matches.get("/team/img")
def get_team_image(team_name: str):
    response = dao_matches_s3.get_team_img(team_name=team_name)
    if response : return StreamingResponse(BytesIO(response), media_type="image/png")
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Team Unknown") 

@router_matches.get("/match/{id_match}")
def get_match_info(id_match: str):
    response = dao_matches_s3.get_infos_match(id_match=id_match)
    if response: return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Match Unknown")

@router_matches.get("/matches")
def get_list_matches(
    id_match: Optional[str] = Query(None),
    before: Optional[str] = Query(None),
    after: Optional[str] = Query(None),
    team: Optional[str] = Query(None),
    event: Optional[str] = Query(None),
    player: Optional[str] = Query(None),
    page: int = Query(1, description="Numéro de la page"),
    page_size: int = Query(10, description="Nombre de résultats par page")
):
    response = dao_matches_s3.filter_matches(id_match, before, after, team_name=team, event_name=event, player_name=player, page=page, page_size=page_size)
    return response

@router_matches.get("/players")
def get_players():
    response = dao_matches_s3.get_players()
    if response: return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Players Unknown")

@router_matches.get("/teams")
def get_teams():
    response = dao_matches_s3.get_teams()
    if response: return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Teams Unknown")

@router_matches.get("/events")
def get_events():
    response = dao_matches_s3.get_events()
    if response: return response
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Events Unknown")