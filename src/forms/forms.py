from fastapi.param_functions import Form
from datetime import datetime

class UserCreateForm:
    def __init__(self, email: str = Form(), username: str = Form(), password: str = Form()):
        self.email = email
        self.username = username
        self.password = password

class UpcomingMatchForm:
    def __init__(self, id_match: str = Form(), start_date: datetime = Form(), red_team_name: str = Form(), blue_team_name: str = Form()):
        self.id_match = id_match
        self.start_date = start_date
        self.red_team_name = red_team_name
        self.blue_team_name = blue_team_name

class MatchForm:
    def __init__(self, id_match: str = Form(), start_date: datetime = Form(), 
                 red_team_name: str = Form(), red_team_score: float = Form(), red_team_goals: float = Form(), red_team_shots: float = Form(), red_team_assists: float = Form(), red_team_saves: float = Form(), red_team_demos: float = Form(), 
                 blue_team_name: str = Form(), blue_team_score: float = Form(), blue_team_goals: float = Form(), blue_team_shots: float = Form(), blue_team_assists: float = Form(), blue_team_saves: float = Form(), blue_team_demos: float = Form()):
        self.id_match = id_match
        self.start_date = start_date
        self.red_team_name = red_team_name
        self.red_team_score = red_team_score
        self.red_team_goals = red_team_goals
        self.red_team_shots = red_team_shots
        self.red_team_assists = red_team_assists
        self.red_team_saves = red_team_saves
        self.red_team_demos = red_team_demos
        self.blue_team_name = blue_team_name
        self.blue_team_score = blue_team_score
        self.blue_team_goals = blue_team_goals
        self.blue_team_shots = blue_team_shots
        self.blue_team_assists = blue_team_assists
        self.blue_team_saves = blue_team_saves
        self.blue_team_demos = blue_team_demos


class UserBetForm:
    def __init__(self, id_match: str = Form(), team_name: str = Form(), amount: int = Form()):
        self.id_match = id_match
        self.team_name = team_name
        self.amount = amount

class HistoryMatchForm:
    def __init__(self, team_name_1: str = Form(), team_name_2: str = Form()):
        self.team_name_1 = team_name_1
        self.team_name_2 = team_name_2