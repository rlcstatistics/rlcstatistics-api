from pydantic import BaseModel
from datetime import datetime

class BetMatch(BaseModel):
    id_match: str
    team_name: str
    amount: int
    date: datetime

class MatchOdds(BaseModel):
    id_match: str
    red_team_name: str
    red_sum_amount: int
    red_odd: float
    blue_team_name: str
    blue_sum_amount: int
    blue_ood: float

class TeamMatchOdds(BaseModel):
    id_match: str
    team_name: str
    sum_amount: int