from typing import Optional, Dict
from pydantic import BaseModel
    
class Bet(BaseModel):
    uuid: str
    date: str
    id_match: str
    amount: int
    team_name: str
    reward: Optional[int]

class User(BaseModel):
    uuid: str
    username: str
    email: str
    role: str
    creation_date: str
    refill_date: str
    coins: int

class UserWithBets(User):
    bets: Dict[str, Bet]

class UserPassword(User):
    hashed_password: str

class UserAll(UserPassword):
    pass

class UserLeaderboard(BaseModel):
    email: str
    username: str
    coins: str
