from pydantic import BaseModel
from datetime import datetime


class UpcomingMatch(BaseModel):
    id_match: str
    start_date: str
    team_name_orange: str
    team_name_blue: str

class Match(UpcomingMatch):
    orange_team_games_win: int
    orange_team_score: float
    orange_team_goals: float
    orange_team_shots: float
    orange_team_assists: float
    orange_team_saves: float
    orange_team_demos: float
    blue_team_games_win: int
    blue_team_score: float
    blue_team_goals: float
    blue_team_shots: float
    blue_team_assists: float
    blue_team_saves: float
    blue_team_demos: float

class Team(BaseModel):
    name: str
    score: float | str
    goals: float | str
    shots: float | str
    assists: float | str
    saves: float | str

class UpcomingMatchInfo(BaseModel):
    id_match: str
    start_date: str
    blue: Team | None
    orange: Team | None
    blue_odd: float
    orange_odd: float