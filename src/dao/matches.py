import os, json
from io import BytesIO
from google.cloud import storage
from typing import Optional
from datetime import datetime

import pandas as pd
from schemas.matches import Match, UpcomingMatch, Team, UpcomingMatchInfo

from logger import logger

class DAOMatchesGCS():
    def __init__(self):
        self.BUCKET_NAME = "rlcstatistics"
        self.client = storage.Client()
        self.bucket = self.client.bucket(self.BUCKET_NAME)
        self.ENV = os.getenv("ENV", "dev")

    def get_match_from_uuid(self, uuid: str) -> Optional[Match]:
        try:
            user_data = json.loads(
                self.client.bucket(self.BUCKET_NAME)
                .blob(f'{self.ENV}/matches/status=done/{uuid}')
                .download_as_text()
            )
            return Match(**user_data)
        except Exception as e:
            logger.error(e)
            return None
        
    def get_upcoming_matches(self) -> Optional[list[UpcomingMatch]]:
        upcoming_matches: list[UpcomingMatch] = []
        current_datetime = datetime.now()

        blobs = [blob for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/matches/status=upcoming') if blob.name[-1] != "/"]
        for blob in blobs:
            args = {key: value for key, value in [pair.split('=') for pair in blob.name.split("/")[-1].split('&')]}
            upcoming_match = UpcomingMatch(**args)
            if datetime.strptime(upcoming_match.start_date, "%Y-%m-%dT%H:%M:%SZ") > current_datetime:
                upcoming_matches.append(upcoming_match)
        if len(upcoming_matches) == 0:
            return None
        upcoming_matches.sort(key=lambda x: datetime.strptime(x.start_date, "%Y-%m-%dT%H:%M:%SZ"))
        return upcoming_matches

    def get_upcoming_match(self, id_match: str) -> Optional[UpcomingMatchInfo]:
        blobs = [blob for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/matches/status=upcoming') if id_match in blob.name]
        if len(blobs) < 1 or len(blobs) > 2: return None
        args = {key: value for key, value in [pair.split('=') for pair in blobs[0].name.split("/")[-1].split('&')]}
        upcoming_match = UpcomingMatch(**args)

        odds = self.convert_sum_bets_to_odds(
            self.get_sum_bets_match(id_match=id_match, team_name=upcoming_match.team_name_blue), 
            self.get_sum_bets_match(id_match=id_match, team_name=upcoming_match.team_name_orange)
        )
        return UpcomingMatchInfo(
            id_match=id_match,
            start_date=upcoming_match.start_date,
            blue=self.get_infos_team(team_name=upcoming_match.team_name_blue),
            orange=self.get_infos_team(team_name=upcoming_match.team_name_orange),
            blue_odd=odds["blue"],
            orange_odd=odds["orange"]
        )

    def get_infos_team(self, team_name: str) -> Team:
        try:
            averages = pd.read_csv(BytesIO(self.bucket.blob(f'{self.ENV}/teams/{team_name}.csv').download_as_bytes())).mean()
            return Team(name=team_name, **averages.fillna("?"))
        except Exception as e:
            logger.error(e)
            return Team(name=team_name, score="?", goals="?", shots="?", assists="?", saves="?", demos="?")

    def get_team_img(self, team_name: str):
        try:
            return self.bucket.blob(f'{self.ENV}/teams/{team_name}.png').download_as_bytes()
        except Exception as e:
            return self.bucket.blob(f'{self.ENV}/teams/default.png').download_as_bytes()
        
    def get_infos_match(self, id_match: str):
        try:
            return json.loads(self.bucket.blob(f'{self.ENV}/matches/status=done/id_match={id_match}').download_as_string())
        except Exception as e:
            return None

    def get_sum_bets_match(self, id_match: str, team_name: str) -> int:
        return sum([int(blob.name.split('||coins=')[-1]) for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/bets/matches/id_match={id_match}/team_name={team_name}')])
    
    def convert_sum_bets_to_odds(self, blue_sum: int, orange_sum: int):
        sum = blue_sum + orange_sum + 200
        return {"blue": round(1/((blue_sum+100)/sum), 2), "orange": round(1/((orange_sum+100)/sum), 2)}
    
    def filter_matches(self, id_match=None, before=None, after=None, team_name=None, event_name=None, player_name=None, page=1, page_size=10):
        filtered_matches = []
        index = json.loads(self.bucket.blob(f'{self.ENV}/matches/index.json').download_as_string())
        
        for match in index:
        # Conditions de filtrage
            if (id_match is None or match['id_match'] == id_match) and \
            (before is None or datetime.strptime(match['date'], "%Y-%m-%dT%H:%M:%SZ") < datetime.strptime(before, "%Y-%m-%dT%H:%M:%SZ")) and \
            (after is None or datetime.strptime(match['date'], "%Y-%m-%dT%H:%M:%SZ") > datetime.strptime(after, "%Y-%m-%dT%H:%M:%SZ")) and \
            (team_name is None or team_name in [match['blue']['team'], match['orange']['team']]) and \
            (event_name is None or match['event'] == event_name) and \
            (player_name is None or player_name in match['blue']['players'] or player_name in match['orange']['players']):
                
                filtered_matches.append(match)

        filtered_matches.sort(key=lambda x: datetime.strptime(x['date'], "%Y-%m-%dT%H:%M:%SZ"), reverse=True)
        
        total_count = len(filtered_matches)
        start_index = (page - 1) * page_size
        end_index = start_index + page_size
        paginated_matches = filtered_matches[start_index:end_index]
        
        return {
            "matches": paginated_matches,
            "page": page,
            "perPage": page_size,
            "totalCount": total_count
        }
    
    def get_players(self):
        try:
            bytes_data  = self.bucket.blob(f'{self.ENV}/matches/players.txt').download_as_string()
            text_data = bytes_data.decode('utf-8')
            return [line.strip() for line in text_data.split('\n') if line.strip()]
        except Exception as e:
            logger.error(e)
            return None
        
    def get_teams(self):
        try:
            bytes_data  = self.bucket.blob(f'{self.ENV}/matches/teams.txt').download_as_string()
            text_data = bytes_data.decode('utf-8')
            return [line.strip() for line in text_data.split('\n') if line.strip()]
        except Exception as e:
            logger.error(e)
            return None
    
    def get_events(self):
        try:
            bytes_data  = self.bucket.blob(f'{self.ENV}/matches/events.txt').download_as_string()
            text_data = bytes_data.decode('utf-8')
            return [line.strip() for line in text_data.split('\n') if line.strip()]
        except Exception as e:
            logger.error(e)
            return None