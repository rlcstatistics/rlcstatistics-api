from io import StringIO
import re
from openai import OpenAI, OpenAIError
from fastapi import HTTPException, status
from typing import Optional
import json, os
from google.cloud import storage
import pandas as pd
from schemas.users import Bet, User, UserAll, UserLeaderboard
from datetime import datetime, timedelta
from uuid import uuid4
from functions.utils import hash_password

from logger import logger

class DAOUserGCS:
    def __init__(self):
        self.BUCKET_NAME = "rlcstatistics"
        self.client = storage.Client()
        self.bucket = self.client.bucket(self.BUCKET_NAME)
        self.ENV = os.getenv("ENV", "dev")
        self.openai_client = OpenAI(api_key=os.getenv("OPENAI_API_KEY", ""))

    def get_user_from_email(self, email: str) -> Optional[UserAll]:
        try:
            blob = self.bucket.blob(f'{self.ENV}/users/{email}')
            user_data = json.loads(blob.download_as_text())
            return UserAll(**user_data)
        except Exception as e:
            logger.error(e)
            return None

    def create_user(self, email: str, username: str, password: str) -> Optional[User]:
        existing_user = self.get_user_from_email(email=email)
        if existing_user:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail='Account already exists.')
        if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail='Email pattern doesn\'t match.')
                
        try:
            response = self.openai_client.moderations.create(input=f"{email.split(sep='@')[0]} {username}", model='text-moderation-stable')
            if any(value[1] > 0.01 for value in response.results[0].category_scores):
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail='Your email or username is not appropriate.')
        except OpenAIError as openai_error:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f'OpenAI API error: {str(openai_error)}')
        except HTTPException as http_error:
            raise http_error
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f'Something goes wrong: {str(e)}')

        content = {
            'uuid' : str(uuid4()),
            "username" : username,
            "email" : email,
            'role': "USER",
            'creation_date': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'refill_date' : (datetime.today() - timedelta(days=1)).strftime('%Y%m%d'),
            'coins': 100,
            "hashed_password" : hash_password(password)
        }
        blob = self.bucket.blob(f'{self.ENV}/users/{email}')
        blob.upload_from_string(json.dumps(content), content_type='application/json')

        return User(**content)
        
    def delete_user_by_email(self, email: str) -> None:
        try:
            self.delete_user_profile(email=email)
            self.delete_user_bets(email=email)
            self.delete_user_current_bets(email=email)
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Account is not delete')
        
    def delete_user_profile(self, email: str) -> None:
        try:
            self.bucket.blob(f'{self.ENV}/users/{email}').delete()
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Profile is not delete')

    def delete_user_bets(self, email: str) -> None:
        try:
            [blob.delete() for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/bets/users/{email}')]
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Bets are not delete')

    def delete_user_current_bets(self, email: str) -> None:
        try:
            [blob.delete() for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/bets/matches') if email in blob.name]
        except Exception as e:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Current bets are not delete')
    
    def refill_user(self, user: User) -> Optional[User]:
        current_time = datetime.now()
        if user.refill_date == current_time.strftime('%Y%m%d'):
            return None
        user.coins += 100
        user.refill_date = current_time.strftime('%Y%m%d')
        blob = self.bucket.blob(f'{self.ENV}/users/{user.email}')
        blob.upload_from_string(json.dumps(user.dict()), content_type='application/json')

        return User(**user.dict())
    
    def do_bet(self, user: User, id_match: str, team_name: str, amount: int) -> Bet:
        if amount > 10000:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f'Bet with more than 10 000 coins is not allowed.')
        if user.coins < amount:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f'No enough coins for this bet.')
        self.already_match_start(id_match)
        self.already_do_bet(id_match, user.email)
        
        current_date, bet_uuid = datetime.now(), str(uuid4())
        user.coins -= amount
        bet = {
            'uuid' : bet_uuid,
            "date" : current_date.strftime('%Y-%m-%d %H:%M:%S'),
            'id_match' : id_match,
            'amount' : amount,
            'team_name' : team_name
        }
        self.bucket.blob(f'{self.ENV}/bets/users/{user.email}/{id_match}').upload_from_string(json.dumps(bet), content_type='application/json')
        self.bucket.blob(f'{self.ENV}/users/{user.email}').upload_from_string(json.dumps(user.dict()), content_type='application/json')
        self.bucket.blob(f'{self.ENV}/bets/matches/id_match={id_match}/team_name={team_name}/{user.email}||coins={amount}').upload_from_string('')
        return Bet(**bet)

    def already_match_start(self, id_match: str) -> bool:
        blobs = [blob for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/matches/status=upcoming') if id_match in blob.name]
        if len(blobs) != 1:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f'There is something wrong with this match (duplicates uuid).')
        if datetime.strptime(blobs[0].name.split("=")[-1], "%Y-%m-%dT%H:%M:%SZ") < datetime.now():
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f'The match has already started or is over.')
    
    def already_do_bet(self, id_match: str, email: str) -> bool:
        blobs = [blob for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/bets/users/{email}') if id_match in blob.name]
        if len(blobs) != 0:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f'You have already made a bet for this match.')
    
    def get_user_bets(self, email: str) -> list[Bet]:
        list_bets = [Bet(**json.loads(self.bucket.blob(blob.name).download_as_text())) for blob in self.client.list_blobs(self.BUCKET_NAME, prefix=f'{self.ENV}/bets/users/{email}')]
        return sorted(list_bets, key=lambda x: datetime.strptime(x.date, '%Y-%m-%d %H:%M:%S'), reverse=True)[:10]
    
    def get_leaderboard(self) -> list[UserLeaderboard]:
        leaderboard_df = pd.read_csv(StringIO(self.bucket.blob(f'{self.ENV}/leaderboard.csv').download_as_text()))
        return [UserLeaderboard(email=row['email'], username=row['username'], coins=row['coins']) for _, row in leaderboard_df.iterrows()][:10]
