rm -rf .venv
virtualenv .venv
source .venv/bin/activate
pip install --no-cache-dir -r requirements.txt